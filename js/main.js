window.$(document).ready(function () {
  // Modal form pop up
  var $signUpBtn = $("#signUpBtn, .btn__custom-input");
  var $signUpBtnClose = $(".sign-up__close, .sign-up__button");
  var $signUpForm = $(".sign-up__form");
  var $overlay = $(".overlay");
  var $customInput = $(".custom-input__input");

  $signUpBtn.click(function () {
    $overlay.addClass("show");
    $signUpForm.addClass("show");
  });

  $customInput.click(function () {
    $overlay.addClass("show");
    $signUpForm.addClass("show");
  });

  $signUpBtnClose.click(function () {
    $overlay.removeClass("show");
    $signUpForm.removeClass("show");
  });


})